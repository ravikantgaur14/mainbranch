package com.synechron;

import java.math.BigInteger;
import java.util.Scanner;

public class PrimeNumberCheckWIthBigInteger {

	public static void main(String[] args) {
		
			/*Scanner sc=new Scanner(System.in);
			System.out.println("Enter the base value:");
			String base=sc.next();
		    System.out.println("Enter the base to the power value:");
		    int power=sc.nextInt();
		    BigInteger BASE = new BigInteger(base);
		    BigInteger NUMBER =BASE.pow(power);
		    PrimeNumberCheckWIthBigInteger primeNumberObj = new PrimeNumberCheckWIthBigInteger();
		    if(primeNumberObj.isPrimeNumber(NUMBER)){
		    	System.out.println("2^"+power+"="+NUMBER+", is a prime number.");
		    }else{
		    	System.out.println("2^"+power+"="+NUMBER+", is not a prime number.");
		    }*/
		
		String s1="String";
		String s2=new String("String");
		String s3=s1.intern();
		System.out.println(s1==s3);
		
		String s11="Strings".concat("Ex");
		String s22=s11.intern();
		System.out.println(s11==s22);
		
		String s4="ja".concat("va");
		System.out.println(s4);
		String s5=s4.intern();
		System.out.println(s4==s5);
		
		String s6="vo".concat("id");
		String s7=s6.intern();
		System.out.println(s6==s7);
		
	/*	String s8="ma".concat("in");
		String s9=s8.intern();
		System.out.println(s8==s9);*/
		
		String s8="fl".concat("oat");
		String s9=s8.intern();
		System.out.println(s8==s9);
		
		
	}
	
	public boolean isPrimeNumber(BigInteger number){
         
		 BigInteger temp=number.divide(new BigInteger("2"));
		 BigInteger i=new BigInteger("2");
		 boolean flag=true;
		 while((i.equals(temp) || (i.compareTo(temp)==-1))){
			 
			 if(number.remainder(i).equals(new BigInteger("0"))){
				 flag=false;
				 break;
	         }
			i=i.add(new BigInteger("1"));
		 }
		 return flag;
	}
}
