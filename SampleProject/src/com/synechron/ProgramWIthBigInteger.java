package com.synechron;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class ProgramWIthBigInteger {

	public static void main(String[] args) {
		
		 Scanner sc=new Scanner(System.in);
		 System.out.println("Enter value N1:");
		 String input=sc.next();
		 BigInteger N1 = new BigInteger(input);
		 System.out.println("Enter divisor value P:");
		 input=sc.next();
		 BigInteger P = new BigInteger(input);
		 BigInteger R1=N1.remainder(P);
		 System.out.println("");
		 System.out.print("N1%P : "+N1+"%"+P);
		 System.out.println(" ==> Remainder(R1) :"+R1);
		 System.out.println("");
		 
		 if(!R1.equals(new BigInteger("0"))){
			 
			 ArrayList<BigInteger> remainderList=new ArrayList<BigInteger>();
			 remainderList.add(R1);
			 BigInteger remainder=R1;
			 BigInteger index=new BigInteger("2");
			 while(true){
				 BigInteger n=remainder.multiply(remainder).multiply(new BigInteger("2")).subtract(new BigInteger("1")); // formula: 2*(r*r) -1
				 remainder=n.remainder(P);
				 System.out.println("");
				 System.out.print("N"+index+"%P : "+n+"%"+P);
				 System.out.println(" ==> Remainder(R"+index+") :"+remainder);
				 System.out.println("");
				 
				 if(remainder.equals(new BigInteger("0")) || remainderList.contains(remainder)){ // loop exit condition
					 System.out.println("Stopped! The Infinite Series Remainder Calculation, Because Either Remainder Value Came As 0 Or Repetitive.");
					 if(remainderList.contains(remainder)){
						 System.out.println("Note: Last Remainder And Remainder (R"+(remainderList.indexOf(remainder)+1)+") Is Repetitive In The Infinite Series Remainder Calculation.");
						 remainderList.indexOf(remainder);
					 }
					 break;
				 }else{
					 remainderList.add(remainder);
					 index=index.add(new BigInteger("1"));
				 }
			 }
		 }
	}
}

