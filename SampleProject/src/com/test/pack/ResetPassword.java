package com.test.pack;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.Scanner;

public class ResetPassword {
	
		private static Scanner input=new Scanner(System.in); 
		private static String filePath="E:/user1.properties"; 
		public static void main(String[] args) { 
			System.out.println("Please Enter your user name:"); 
			String userName= input.nextLine(); 
			System.out.println("Please Enter your old password:"); 
			String password= input.nextLine(); 
			boolean isLogin = login(userName, password); 
			if(isLogin){ 
				System.out.println("Enter new password:"); 
				String newPassword = input.nextLine(); 
				System.out.println("Confirm password:"); 
				String confirmPassword = input.nextLine(); 
				if(newPassword.equals(confirmPassword)){ 
				
					Properties properties=getProperties(); 
					properties.setProperty(userName, newPassword); 
					saveProperties(properties); 
					System.out.println("Password Reset Successfully!"); 
				}else{ 
					System.out.println("New password and confirm password doesn�t match"); 
				} 
			}else{ 
				System.out.println("Sorry! Invalid Username Password."); 
			} 
		} 

		public static boolean login(String userName, String password){ 
			boolean isLogin=false; 
			
			Properties properties=getProperties(); 
			String actualPassword=properties.getProperty(userName); 
			if(password.equals(actualPassword)) 
				isLogin=true; 

			return isLogin; 
		} 

		private static Properties getProperties(){ 
			Properties properties = null; 
			try{ 
				properties=new Properties(); 
				FileInputStream fis= new FileInputStream(filePath); 
				properties.load(fis); 
				fis.close(); 
			}catch (Exception e) { 
				e.printStackTrace(); 
			} 
			return properties; 
		} 

		private static void saveProperties(Properties properties){ 
			try{ 
				FileOutputStream fos= new FileOutputStream(filePath); 
				properties.store(fos, null); 
			}catch (Exception e) { 
				e.printStackTrace(); 
			} 
		} 


	}


