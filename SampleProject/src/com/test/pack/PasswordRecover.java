package com.test.pack;

import java.io.FileInputStream; 
import java.util.Properties; 
import java.util.Scanner; 

public class PasswordRecover { 
	public static void main(String[] args) { 
		PasswordRecover passwordRecover=new PasswordRecover(); 
		passwordRecover.displayPassword(); 
	} 
	public void displayPassword(){ 
		try{ 
			System.out.println("Did you forget your password? Yes/No"); 
			Scanner input=new Scanner(System.in); 
			String isForgot=input.nextLine(); 
			if(isForgot.equalsIgnoreCase("Yes")){ 
				System.out.println("Please Enter the User Name"); 
				String username=input.nextLine(); 
				int userID=0; 
				String answer=""; 
				boolean flag=true; 
				Properties properties=new Properties(); 
				properties.load(new FileInputStream("E:/user.properties")); 
				String userIds=properties.getProperty("USERID"); 
				for(String userId:userIds.split(",")){ 
					if(userId.split(":")[1].equalsIgnoreCase(username)){ 
						userID=Integer.parseInt(userId.split(":")[0]); 
					} 
				} 
				String questionsMap=properties.getProperty("SecurityQuestionMapping"); 
				for(String questions:questionsMap.split(",")){ 
					if(userID==Integer.parseInt(questions.split(":")[0])){ 
						if(flag){ 
							System.out.println(questions.split(":")[1].split("~")[0]); 
							answer=input.nextLine(); 
							if(answer.equalsIgnoreCase(questions.split(":")[1].split("~")[1])){ 
								flag=true; 
							} 
							else{ 
								flag=false; 
							} 
						}else{ 
							System.out.println("Wrong answer!!"); 
						} 
					} 
				} 
				if(flag){ 
					System.out.println("Your Password is: "); 
					String passwords=properties.getProperty("PASSWORD"); 
					for(String password:passwords.split(",")){ 
						if(userID==Integer.parseInt(password.split(":")[0])){ 
							System.out.println(password.split(":")[1]); 
						} 
					} 
				}else{ 
					System.out.println("Wrong answer!!"); 
				} 
			}else{ 
				System.out.println("Welcome!!"); 
			} 
		}catch(Exception e){ 
			System.out.println(e); 
			e.printStackTrace(); 
		} 
	} 
} 


